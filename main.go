package main

import (
	"fmt"
	"github.com/fatih/color"
	"net"
	"os"
	"strings"
	"time"
	"unicode/utf8"
)

func timer() {
	for i := 0; i < 30; i++ {
		fmt.Printf("\rЭто окно закроется через 30/%d", i)
		time.Sleep(1 * time.Second)
	}

}
func ipAndHost() (host, addrsString string) {
	var v4Addrs []net.IP
	host, _ = os.Hostname()
	addrs, _ := net.LookupIP(host)
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			v4Addrs = append(v4Addrs, addr)
		}
	}
	for _, valIp := range v4Addrs {
		addrsString += valIp.String() + " "
	}
	return
}

func main() {
	hostName, ipAddres := ipAndHost()
	cyanColor := color.New(color.FgCyan)
	grenColor := color.New(color.FgGreen)
	strOfSharp := strings.Repeat("#", utf8.RuneCountInString(hostName+ipAddres+"# ИМЯ"+
		" КОМПЬЮТЕРА--> "+" # IP АДРЕС--> #"))
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println()
	fmt.Println(strOfSharp)
	fmt.Printf("# ")
	grenColor.Printf("ИМЯ КОМПЬЮТЕРА--> ")
	cyanColor.Printf(hostName)
	fmt.Printf(" |")
	grenColor.Printf(" IP АДРЕС--> ")
	cyanColor.Printf(ipAddres[:len(ipAddres)-1])
	fmt.Printf(" #\n")
	fmt.Println(strOfSharp)
	timer()
}
